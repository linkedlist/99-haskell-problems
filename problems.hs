myLast [] = error "Empty list"
myLast (x:[]) = x
myLast (x:y) = myLast y

myButLast [] = error "Empty list"
myButLast (x:[]) = error "List containing just one member"
myButLast (x:y:[]) = x
myButLast (x:y) = myButLast y

kElem :: Num b => [a] -> b -> a
kElem [] _ = error "Empty list or not long enough"
kElem (x:_) 1 = x
kElem (x:t) n = kElem t (n-1)

length' l = length'' l 0 

length'' [] n = n
length'' (x:t) n = length'' t (n+1)

reverse' list = reverse'' list []

reverse'' [] reversed = reversed
reverse'' (h:t) reversed = reverse'' t (h:reversed)


palindrome [] = error "Empty list"
palindrome x = similar (take (quot (length x) 2) x) (take (quot (length x) 2) (reverse' x))


similar [] [] = True
similar _ []  = False
similar [] _  = False
similar (h:t) (x:y) = if h == x then similar t y
								else False

data NestedList a = Elem a | List [NestedList a]

flatten (Elem a) = [a]
flatten (List (x:xs)) = flatten x ++ flatten (List xs)
flatten (List []) = []

compress [] = []
compress (x:y:t) = if x == y then compress (x:t)
							 else [x] ++ compress (y:t)
compress (x:[]) = [x]							 